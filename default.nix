{ nixpkgs ? import <nixpkgs> {} }:
with nixpkgs.pkgs;
let
  tex = texlive.combine {
    inherit (texlive) euenc graphics l3experimental l3kernel l3packages latexmk
    physics scheme-minimal xetex xetex-def amsmath; # ms
  };
in
  stdenv.mkDerivation rec {
    name = "GitLab_Tex_Test";
    src = ./.;

    buildInputs = [
      tex
    ];

    buildPhase = ''
      latexmk --xelatex --shell-escape ${name}.tex
    '';

    installPhase = ''
      mkdir -p $out
      cp ${name}.pdf $out
    '';
  }
